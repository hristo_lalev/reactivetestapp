package te.reacive.hdla.reactivetestapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CoreSelectViewAdapter extends RecyclerView.Adapter<CoreViewHolder> {
    private int cores[];

    public CoreSelectViewAdapter(int[] cores) {
        this.cores = cores;
    }

    @NonNull
    @Override
    public CoreViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // create a new view
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.horizontal_lv_item, viewGroup, false);

        CoreViewHolder holder = new CoreViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull CoreViewHolder coreViewHolder, int i) {
        coreViewHolder.corePicture.setImageResource(cores[i]);

    }

    @Override
    public int getItemCount() {
        return cores.length;
    }


}
