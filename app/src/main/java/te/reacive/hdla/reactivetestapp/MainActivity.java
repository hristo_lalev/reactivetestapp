package te.reacive.hdla.reactivetestapp;

import android.graphics.Color;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.graphics.drawable.Animatable2Compat;
import android.support.graphics.drawable.AnimatedVectorDrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.drawable.AnimatedStateListDrawableCompat;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.jakewharton.rxbinding3.widget.RxSeekBar;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class MainActivity extends AppCompatActivity {
    AnimatedVectorDrawable vd;
    AnimatedVectorDrawable vd_back;
    AnimatedVectorDrawableCompat vdComp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayout layout = findViewById(R.id.linear);
        layout.setBackgroundColor(Color.BLACK);

//        ColorPickerView colorPicker = findViewById(R.id.color_picker);
//        colorPicker.setOnColorChangedListener(new ColorPickerView.OnColorChangedListener() {
//            @Override
//            public void colorChanged(int color) {
//                layout.setBackgroundColor(color);
//            }
//        });

        ImageView animatedArrow = findViewById(R.id.animated_arrow);
        Drawable dr = animatedArrow.getDrawable();
        ImageView animatedArrowb = findViewById(R.id.animated_arrow_back);
        Drawable drb = animatedArrowb.getDrawable();

        if(dr instanceof AnimatedVectorDrawable){
            vd = (AnimatedVectorDrawable)dr;
            vd_back = (AnimatedVectorDrawable)drb;
            vd.registerAnimationCallback(new Animatable2.AnimationCallback(){
                @Override
                public void onAnimationEnd(Drawable drawable) {
                    super.onAnimationEnd(drawable);
                    new Handler().postDelayed(() -> vd.start(),1000);
                }
            });
            vd_back.registerAnimationCallback(new Animatable2.AnimationCallback(){
                @Override
                public void onAnimationEnd(Drawable drawable) {
                    super.onAnimationEnd(drawable);
                    new Handler().postDelayed(() -> vd_back.start(),1000);
                }
            });

            vd_back.start();
            vd.start();


        }else if(dr instanceof AnimatedStateListDrawableCompat){
            vdComp = (AnimatedVectorDrawableCompat)dr;

            ((AnimatedVectorDrawable) dr).registerAnimationCallback(new Animatable2.AnimationCallback(){
                @Override
                public void onAnimationEnd(Drawable drawable) {
                    super.onAnimationEnd(drawable);
                    runOnUiThread(()->vdComp.start());
                }
            });
            vdComp.start();
        }

    //    setContentView(new ColorPickerView(this, l, Color.BLACK));
//        SeekBar sb = findViewById(R.id.seekBar);
//
//        RxSeekBar.userChanges(sb)
//                .startWith(integera->{
//
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(integer -> {
//
//                },
//                error->{
//
//                });

    }
}
