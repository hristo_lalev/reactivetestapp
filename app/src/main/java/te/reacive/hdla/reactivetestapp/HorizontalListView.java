package te.reacive.hdla.reactivetestapp;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import androidx.annotation.Nullable;

public class HorizontalListView extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.horizontal_list_view_layout);
        int  Images[] = {R.drawable.prizm_board,R.drawable.prizm_board,
                R.drawable.prizm_board,R.drawable.prizm_board,R.drawable.prizm_board,R.drawable.prizm_board,R.drawable.prizm_board,R.drawable.prizm_board};

        CoreSelectViewAdapter adapter = new CoreSelectViewAdapter(Images);
        RecyclerView rv = findViewById(R.id.rv_horizontal);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));

    }
}
