package te.reacive.hdla.reactivetestapp;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class ColorPickerView extends View {
    private Paint mPaint;
    private Paint mCenterPaint;
    private Paint mLineMarker;
    private int[] mColors;
    private boolean mTrackingCenter;
    private boolean mHighlightCenter;
    private float angle;
    private float x;
    private float y;

    ColorPickerView(Context c) {
        super(c);
        init();
    }

    ColorPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    ColorPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        mColors = new int[] {
                0xFFFFFF00,
                0xFF00FF00,
                0xFF00FFFF,
                0xFF0000FF,
                0xFFFF00FF,
                0xFFFF0000,
                0xFFFFFF00,
        };
        Shader s = new SweepGradient(0, 0, mColors, null);
        Shader s1 = new Shader();

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setShader(s);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(dpToPx(64));

        mLineMarker =  new Paint(Paint.ANTI_ALIAS_FLAG);
        mLineMarker.setShader(s1);
        mLineMarker.setColor(Color.BLACK);
        mLineMarker.setStrokeWidth(dpToPx(10));
        mLineMarker.setStyle(Paint.Style.STROKE);


        mCenterPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCenterPaint.setColor(Color.BLUE);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float r = CENTER_X - mPaint.getStrokeWidth()*0.5f;

        canvas.translate(CENTER_X, CENTER_X);
        RectF rectf = new RectF(-r, -r, r, r);

        canvas.drawOval(rectf, mPaint);
        canvas.drawCircle(0, 0, CENTER_RADIUS, mCenterPaint);
        //rectf.
        if(!mTrackingCenter) {
             angle = (float) java.lang.Math.atan2(y, x);
        }
        float halfStroke = mPaint.getStrokeWidth() * 0.5f;

        float startX = (rectf.right - halfStroke) * (float) Math.cos((double) angle);
        float startY = (rectf.right - halfStroke) * (float) Math.sin((double) angle);
        float endX = (rectf.right + halfStroke) * (float) Math.cos((double) angle);
        float endY = (rectf.right + halfStroke) * (float) Math.sin((double) angle);

        canvas.drawLine(startX, startY, endX, endY, mLineMarker);


        if (mTrackingCenter) {
            int c = mCenterPaint.getColor();
            mCenterPaint.setStyle(Paint.Style.STROKE);

            if (mHighlightCenter) {
                mCenterPaint.setAlpha(0xFF);
            } else {
                mCenterPaint.setAlpha(0x80);
            }
            canvas.drawCircle(0, 0,
                    CENTER_RADIUS + mCenterPaint.getStrokeWidth(),
                    mCenterPaint);

            mCenterPaint.setStyle(Paint.Style.FILL);
            mCenterPaint.setColor(c);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(CENTER_X*2, CENTER_Y*2);
    }

    private int CENTER_X = dpToPx(132);
    private int CENTER_Y = dpToPx(132);
    private int CENTER_RADIUS = dpToPx(60);

    private int floatToByte(float x) {
        int n = java.lang.Math.round(x);
        return n;
    }
    private int pinToByte(int n) {
        if (n < 0) {
            n = 0;
        } else if (n > 255) {
            n = 255;
        }
        return n;
    }
    private int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private int ave(int s, int d, float p) {
        return s + java.lang.Math.round(p * (d - s));
    }

    private int interpColor(int colors[], float unit) {
        if (unit <= 0) {
            return colors[0];
        }
        if (unit >= 1) {
            return colors[colors.length - 1];
        }

        float p = unit * (colors.length - 1);
        int i = (int)p;
        p -= i;

        // now p is just the fractional part [0...1) and i is the index
        int c0 = colors[i];
        int c1 = colors[i+1];
        int a = ave(Color.alpha(c0), Color.alpha(c1), p);
        int r = ave(Color.red(c0), Color.red(c1), p);
        int g = ave(Color.green(c0), Color.green(c1), p);
        int b = ave(Color.blue(c0), Color.blue(c1), p);

        return Color.argb(a, r, g, b);
    }

    private int rotateColor(int color, float rad) {
        float deg = rad * 180 / 3.1415927f;
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        ColorMatrix cm = new ColorMatrix();
        ColorMatrix tmp = new ColorMatrix();

        cm.setRGB2YUV();
        tmp.setRotate(0, deg);
        cm.postConcat(tmp);
        tmp.setYUV2RGB();
        cm.postConcat(tmp);

        final float[] a = cm.getArray();

        int ir = floatToByte(a[0] * r +  a[1] * g +  a[2] * b);
        int ig = floatToByte(a[5] * r +  a[6] * g +  a[7] * b);
        int ib = floatToByte(a[10] * r + a[11] * g + a[12] * b);

        return Color.argb(Color.alpha(color), pinToByte(ir),
                pinToByte(ig), pinToByte(ib));
    }

    private static final float PI = 3.1415926f;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        x = event.getX() - CENTER_X;
        y = event.getY() - CENTER_Y;

        boolean inCenter = java.lang.Math.hypot(x, y) <= CENTER_RADIUS;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mTrackingCenter = inCenter;
                if (inCenter) {
                    mHighlightCenter = true;
                    invalidate();
                    break;
                }
            case MotionEvent.ACTION_MOVE:
                if (mTrackingCenter) {
                    if (mHighlightCenter != inCenter) {
                        mHighlightCenter = inCenter;
                        invalidate();
                    }
                } else {
                    float angle = (float)java.lang.Math.atan2(y, x);
                    // need to turn angle [-PI ... PI] into unit [0....1]
                    float unit = angle/(2*PI);
                    if (unit < 0) {
                        unit += 1;
                    }
                    mCenterPaint.setColor(interpColor(mColors, unit));
                    invalidate();
                }
                break;
            case MotionEvent.ACTION_UP:
                if (mTrackingCenter) {
//                    if (inCenter && (null != mListener)) {
//                        mListener.colorChanged(mCenterPaint.getColor());
//                    }
                    mTrackingCenter = inCenter;    // so we draw w/o halo
                    invalidate();
                }
                break;
        }
        return true;
    }
}
