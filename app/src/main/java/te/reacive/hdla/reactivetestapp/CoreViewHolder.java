package te.reacive.hdla.reactivetestapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

public class CoreViewHolder extends RecyclerView.ViewHolder {
    public ImageView corePicture;
    public CoreViewHolder(@NonNull View itemView) {
        super(itemView);

        corePicture = itemView.findViewById(R.id.iv_core_picture);
    }
}
